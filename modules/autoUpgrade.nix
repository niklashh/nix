{
  config,
  lib,
  pkgs,
  ...
}:

{
  system.autoUpgrade = {
    # FIXME reboot only if required
    # allowReboot = true; # reboots every time regardless of whether kernel version changed
    # rebootWindow = {
    #   lower = "00:00";
    #   upper = "24:00";
    # };
    # randomizedDelaySec = "2min";
    enable = true;
    dates = "02:30";
    flags = [ "--refresh" ];
    flake = "gitlab:niklashh/nix";
  };
}
