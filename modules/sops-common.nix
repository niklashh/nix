{
  config,
  lib,
  pkgs,
  ...
}:

{
  sops = {
    templates."potato-systems-fi-lego-env" = {
      content = ''
        CF_DNS_API_TOKEN=${config.sops.placeholder."cloudflare-dns/potato-systems.fi/api-token"}
      '';
      owner = "acme";
      group = "acme";
    };
    secrets."cloudflare-dns/potato-systems.fi/api-token" = {
      sopsFile = ../secrets/common.yml;
    };
    secrets."hashedPassword/niklash" = {
      neededForUsers = true;
      sopsFile = ../secrets/common.yml;
    };
  };
}
