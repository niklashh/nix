{
  config,
  lib,
  pkgs,
  ...
}:
let
  pubkey = "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIBN5WMSm46M9bkY3LJwXOL/oDtR8pvVmfT+RIAbT4/eOAAAABHNzaDo= niklash";
in
{
  programs.fish.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.niklash = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "libvirtd"
    ];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [ pubkey ];
    hashedPasswordFile = config.sops.secrets."hashedPassword/niklash".path;
  };
  users.users.root.openssh.authorizedKeys.keys = [ pubkey ];
}
