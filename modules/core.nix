{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "Europe/Helsinki";

  services.xserver.xkb.layout = "us";
  services.xserver.xkb.variant = "colemak";

  console = {
    useXkbConfig = true;
  };

  nix = {
    gc = {
      persistent = true;
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 7d";
    };

    settings = {
      keep-outputs = true;
      keep-derivations = true;
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      auto-optimise-store = true;
    };
  };

  environment.systemPackages =
    with pkgs;
    [
      htop
      emacs-nox
      neovim
      nmap
      iotop
      pciutils
      ssh-to-age
      tmux
    ]
    ++ (with inputs.fablab-nix.packages."x86_64-linux"; [
      hw-info
      upgrade
    ]);

  users.mutableUsers = false;
}
