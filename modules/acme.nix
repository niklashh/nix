{
  config,
  lib,
  pkgs,
  ...
}:

{
  imports = [ ./sops-common.nix ];
  security.acme = {
    acceptTerms = true;
    defaults.email = "niklas.halonen@protonmail.com";
    # defaults.webroot = "/var/lib/acme/acme-challenge";
    defaults.group = "nginx";
    certs."potato-systems.fi" = {
      # Comment out for production server after testing with staging
      # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      dnsProvider = "cloudflare";
      extraDomainNames = [ "*.potato-systems.fi" ];
      environmentFile = config.sops.templates."potato-systems-fi-lego-env".path; # NOTE Requires importing sops-common.nix
    };
  };
}
