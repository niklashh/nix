{ pkgs, inputs, ... }:
{
  imports = [
    ./programs
    ./services
  ];

  nixGL.packages = inputs.nixgl.packages;
  nixGL.defaultWrapper = "mesa";
  nixGL.offloadWrapper = "nvidiaPrime";
  nixGL.installScripts = [
    "mesa"
    "nvidiaPrime"
  ];

  home = {
    username = "niklash";
    homeDirectory = "/home/niklash";

    packages = (with pkgs.nixgl.auto; [ nixGLDefault ]) ++ (with pkgs; [ home-manager ]);

    stateVersion = "24.11";
  };
}
