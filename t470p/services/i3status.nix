{ pkgs, ... }:
{
  wayland.windowManager.sway.config.bars = [
    {
      mode = "dock";
      hiddenState = "hide";
      position = "bottom";
      workspaceButtons = true;
      workspaceNumbers = false;
      statusCommand = "${pkgs.i3status}/bin/i3status";
      fonts = {
        names = [ "monospace" ];
        size = 8.0;
      };
      trayOutput = "primary";
      colors = with (import ../kanagawa-colors.nix); {
        background = sumiInk0;
        statusline = fujiWhite;
        separator = sumiInk4;
        focusedWorkspace = {
          border = waveBlue1;
          background = waveBlue2;
          text = fujiWhite;
        };
        activeWorkspace = {
          border = waveBlue1;
          background = sumiInk1;
          text = fujiWhite;
        };
        inactiveWorkspace = {
          border = sumiInk2;
          background = sumiInk1;
          text = sumiInk4;
        };
        urgentWorkspace = {
          border = "#D35053"; # Lighter autumnRed
          background = autumnRed;
          text = fujiWhite;
        };
        bindingMode = {
          border = "#ECB571"; # Lighter autumnYellow
          background = autumnYellow;
          text = fujiWhite;
        };
      };
    }
  ];
}
