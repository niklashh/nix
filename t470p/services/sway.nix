{
  config,
  lib,
  pkgs,
  ...
}:
{
  wayland.windowManager.sway = {
    enable = true;
    # package = config.lib.nixGL.wrap pkgs.sway;
    # package = null; # Don't override system-installed one

    config = {
      modifier = "Mod4";
      terminal = "${pkgs.kitty}/bin/kitty";

      workspaceAutoBackAndForth = false;

      window.titlebar = true; # May cause some applications to have unwanted title bars
      startup = [
        {
          # https://github.com/ErikReider/SwayNotificationCenter#sway-usage
          command = "${pkgs.swaynotificationcenter}/bin/swaync";
          always = true;
        }
        {
          # TODO kill process first?
          command = "${pkgs.autotiling}/bin/autotiling";
          always = false;
        }
      ];
      keybindings =
        let
          mod = config.wayland.windowManager.sway.config.modifier;
        in
        lib.mkOptionDefault ({
          "${mod}+c" =
            ''exec --no-startup-id mkdir -p ~/Pictures && ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp -d -w 1 -b '#00000022' -s '#ffffff22')" - |tee ~/Pictures/screenshot-$(date +%Y%m%d-%H%M%S).png | ${pkgs.wl-clipboard}/bin/wl-copy -t image/png'';
          "${mod}+Shift+p" =
            ''exec ${pkgs.grim}/bin/grim -g "$(slurp -p)" -t ppm - | ${pkgs.imagemagick}/bin/convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | cut -d ' ' -f 4 | ${pkgs.wl-clipboard}/bin/wl-copy'';
          "${mod}+Shift+t" =
            ''exec --no-startup-id ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp -d -w 1 -b '#00000022' -s '#ffffff22')" - | ${pkgs.tesseract}/bin/tesseract stdin stdout | ${pkgs.wl-clipboard}/bin/wl-copy'';
          "${mod}+m" = "exec --no-startup-id ${pkgs.wl-mirror}/bin/wl-mirror eDP-1";

          "${mod}+h" = "split h";
          "${mod}+v" = "split v";
          "${mod}+f" = "fullscreen toggle";
          "${mod}+comma" = "layout stacking";
          "${mod}+period" = "layout tabbed";
          "${mod}+slash" = "layout toggle split";
          "${mod}+a" = "focus parent";
          "${mod}+shift+r" = "reload";

          "${mod}+Shift+l" = "exec --no-startup-id ${pkgs.swaylock}/bin/swaylock";
          "${mod}+n" = "exec --no-startup-id ${pkgs.swaynotificationcenter}/bin/swaync-client -t -sw";
          "${mod}+p" = "exec --no-startup-id${pkgs.swaytools}/bin/winfocus";

          "XF86AudioMute" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ toggle";
          "XF86AudioRaiseVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ +5%";
          "XF86AudioLowerVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ -5%";

          "${mod}+apostrophe" = "move workspace to output right";
          "${mod}+underscore" = "move container to scratchpad";

          "XF86MonBrightnessUp" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set +10% -e 2";
          "XF86MonBrightnessDown" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 10%- -e 2";

          "${mod}+tab" = "workspace back_and_forth";
        });

      # TODO this is a nice way to handle colors through typix https://gitlab.com/hmajid2301/nixicle/-/blob/512df880d52908a232ad48f7a043c4cbdd0264bc/modules/home/desktops/hyprland/config.nix
      colors = with (import ../kanagawa-colors.nix); {
        unfocused = {
          background = sumiInk1;
          border = sumiInk2;
          childBorder = "#222222";
          indicator = "#292d2e";
          text = oldWhite;
        };
        focused = {
          background = waveBlue2;
          border = waveBlue1;
          childBorder = "#285577";
          indicator = "#2e9ef4";
          text = fujiWhite;
        };
      };
      input = {
        "*" = {
          xkb_layout = "us";
          xkb_variant = "colemak";
        };
      };
    };
  };

  programs.swaylock = {
    enable = true;
    settings = {
      font-size = 24;
      indicator-idle-visible = false;
      indicator-radius = 100;
      line-color = with (import ../kanagawa-colors.nix); sumiInk1;
      show-failed-attempts = true;
    };
  };
}
