{
  fujiWhite = "#DCD7BA";
  oldWhite = "#C8C093";
  sumiInk0 = "#16161D";
  sumiInk1 = "#1F1F28";
  sumiInk2 = "#2A2A37";
  sumiInk3 = "#363646";
  sumiInk4 = "#54546D";
  waveBlue1 = "#223249";
  waveBlue2 = "#2D4F67";
  autumnRed = "#C34043";
  autumnYellow = "#DCA561";
}
