_: {
  programs.git = {
    extraConfig.core.quotepath = "off";
    delta.enable = true;
    enable = true;
    userEmail = "niklas.halonen@protonmail.com";
    userName = "Niklas Halonen";
    signing = {
      key = "E83A373DA5AF5068";
      signByDefault = true;
    };
    ignores = [
      ".devenv"
      ".direnv"
      "node_modules"
    ];
  };
}
