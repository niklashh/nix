{
  pkgs,
  inputs,
  config,
  ...
}:
{
  imports = [
    ./git.nix
    ./gpg.nix
    ./firefox.nix
    ./codium.nix
  ];
}
