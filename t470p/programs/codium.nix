{
  pkgs,
  inputs,
  config,
  ...
}:
{
  programs.vscode = {
    enable = true;
    enableUpdateCheck = false;
    package = pkgs.vscodium;

    userSettings = {
      "files.autoSave" = "off";
      "update.mode" = "none";
      "explorer.confirmDelete" = false;
      "workbench.colorTheme" = "Kanagawa Wave";
      "remote.SSH.useLocalServer" = false;
      "jupyter.askForKernelRestart" = false;
      "security.workspace.trust.enabled" = false;
      "editor.wordWrap" = "on";
      "editor.parameterHints.enabled" = false;
      "editor.inlineSuggest.enabled" = false;
      "latex-workshop.latex.build.forceRecipeUsage" = false;
      "latex-workshop.latex.recipe.default" = "lastUsed";
      "nix.serverPath" = "${pkgs.nil}/bin/nil";
      "nix.formatterPath" = "nixfmt";
      "nix.serverSettings" = {
        "nil" = {
          "diagnostics" = {
            "ignored" = [
              "unused_binding"
              "unused_with"
            ];
          };
          "formatting" = {
            "command" = [ "nixfmt" ];
          };
        };
      };
      "nix.enableLanguageServer" = true;
      "editor.formatOnSave" = true;
      "typst-lsp.exportPdf" = "never";
      "editor.acceptSuggestionOnCommitCharacter" = false;
      "editor.acceptSuggestionOnEnter" = "off";
      "editor.unicodeHighlight.ambiguousCharacters" = false;
      "explorer.confirmDragAndDrop" = false;
    };
    extensions =
      with inputs.nix-vscode-extensions.outputs.extensions.x86_64-linux.forVSCodeVersion config.programs.vscode.package.version; [
        open-vsx.haskell.haskell
        # vscode-marketplace.ms-python.python
        vscode-marketplace.usernamehw.errorlens
        open-vsx.jeanp413.open-remote-ssh
        vscode-marketplace.james-yu.latex-workshop
        open-vsx.nvarner.typst-lsp
        open-vsx.jnoortheen.nix-ide
        open-vsx.metaphore.kanagawa-vscode-color-theme
      ];
  };

}
