{
  description = "Description for the project";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/release-24.11";
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    impermanence.url = "github:nix-community/impermanence";
    fablab-nix.url = "gitlab:otafablab/fablab-nix";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixgl.url = "github:nix-community/nixGL";
    nur.url = "github:nix-community/nur";
  };

  outputs =
    {
      self,
      nixpkgs,
      sops-nix,
      flake-parts,
      home-manager,
      nixgl,
      nur,
      ...
    }@inputs:

    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        # To import a flake module
        # 1. Add foo to inputs
        # 2. Add foo as a parameter to the outputs function
        # 3. Add here: foo.flakeModule
      ];
      systems = [ "x86_64-linux" ];

      flake = {
        nixosConfigurations.nas = nixpkgs.lib.nixosSystem {
          modules = [
            sops-nix.nixosModules.sops
            ./nas/configuration.nix
          ];
          specialArgs = {
            inherit inputs;
          };
        };

        nixosConfigurations.gamma = nixpkgs.lib.nixosSystem {
          modules = [
            ./gamma/configuration.nix
          ];
        };

        homeConfigurations.t470p = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            system = "x86_64-linux";
            config.allowUnfree = true;
            overlays = [
              nur.overlay
              nixgl.overlays.default
            ];
          };
          extraSpecialArgs = { inherit inputs; };
          modules = [ ./t470p ];
        };
      };
    };
}
