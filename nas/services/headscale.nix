{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.services.headscale;
in
{
  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # Only allow PFS-enabled ciphers with AES256
    sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
  };

  services.headscale = {
    enable = true;
    port = 1194; # openvpn port number
    settings = {
      server_url = "https://headscale.potato-systems.fi";
    };
  };

  services.nginx.virtualHosts."headscale.potato-systems.fi" = {
    useACMEHost = "potato-systems.fi";
    addSSL = true;
    extraConfig = ''
      proxy_buffering off;
      add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
    '';
    locations."/" = {
      proxyPass = "http://127.0.0.1:${toString cfg.port}";
      proxyWebsockets = true;
    };
  };
}
