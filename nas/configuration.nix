# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:
{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ../modules/users.nix
    ../modules/acme.nix
    ../modules/ssh.nix
    ../modules/core.nix
    ../modules/autoUpgrade.nix
    ./firewall.nix
    ./services
  ];

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = false; # Does not work on z97k
    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  };

  services.zfs.autoScrub.enable = true;
  networking.hostId = "00000000";

  networking.hostName = "nas"; # Define your hostname.
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Helsinki";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    git
  ];

  # sops = {
  #   defaultSopsFile = ../secrets/${config.networking.hostName}.yaml;
  # };

  system.stateVersion = "24.05"; # Did you read the comment?
}
