{
  config,
  lib,
  pkgs,
  ...
}:

{
  networking = {
    firewall.enable = false;
    nftables = {
      enable = true;
      # https://wiki.nftables.org/wiki-nftables/index.php/Netfilter_hooks
      ruleset = ''
        define LAN_IFACE = "enp3s0"

        table inet filter {
          chain input {
            type filter hook input priority 0; policy drop;

            # accept all loopback packets
            iifname "lo" accept
            # accept all icmp packets
            meta l4proto icmp accept
            # accept all packets that are part of an already-established connection
            ct state established,related accept

            # accept all SSH traffic
            iifname $LAN_IFACE tcp dport ssh accept

            # accept all HTTP traffic
            iifname $LAN_IFACE tcp dport {80,443} accept
          }

          chain forward {
            type filter hook forward priority 0; policy drop;
          }

          chain output {
            type filter hook output priority 0; policy accept;
          }
        }
      '';
    };
  };
}
