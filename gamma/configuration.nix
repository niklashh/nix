{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    device = "/dev/sda"; # or "nodev" for efi only
  };

  services.headscale = {
    enable = true;
    address = "0.0.0.0";
    port = 8080;
    # serverUrl = "https://gamma.potato-systems.fi";
  };

  networking.hostName = "gamma";
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [8080];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = ["sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIBN5WMSm46M9bkY3LJwXOL/oDtR8pvVmfT+RIAbT4/eOAAAABHNzaDo="];
  system.stateVersion = "23.11"; # Did you read the comment?
}
